SUMMARY = "network hardware drivers"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = "\
    kernel-module-nvme \
"
