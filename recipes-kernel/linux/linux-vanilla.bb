DEPENDS += "${@bb.utils.contains('ARCH', 'x86', 'elfutils-native', '', d)}"
DEPENDS += "openssl-native util-linux-native"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|intel-corei7"

KBRANCH ?= "linux-4.17.y"
LINUX_VERSION ?= "4.17.9"
SRCREV ?= "339d95b9a7a4e1c911ed3627307c7ba1e8675228"

SRC_URI = " \
           git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH} \
           file://defconfig \
"

PV = "${LINUX_VERSION}+git${SRCPV}"

inherit kernel
require recipes-kernel/linux/linux-yocto.inc

LIC_FILES_CHKSUM = "file://LICENSES/preferred/GPL-2.0;md5=74274e8a218423e49eefdea80bc55038"

do_configure_prepend() {
    cp ${WORKDIR}/defconfig ${B}/.config
}
