DESCRIPTION = "Start X11"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
    file://startx.service \
"

do_install() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/startx.service ${D}${systemd_system_unitdir}
}

RDEPENDS_${PN} = " \
    gnome-shell \
"

SYSTEMD_SERVICE_${PN} = "startx.service"
