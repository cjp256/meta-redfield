
BRIDGE="brbridged"

if [[ ! -e /sys/class/net/brbridged ]]; then
    brctl addbr brbridged
fi

if [[ ! -e /sys/class/net/eth0 ]]; then
    xl network-attach 0 backend=ndvm bridge=brbridged
fi

sleep 2

brctl addif brbridged eth0
ip link set eth0 up
ethtool --offload eth0 tx off gro off gso off
ethtool --offload brbridged tx off gro off gso off

