DESCRIPTION = "Dom0 GNOME Shell Configurations"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://dconf.d/dconf-user.conf \
    file://dconf-profile \
    file://beam.png \
    file://beam-windows.png \
    file://beam-linux.png \
    file://beam-firefox.png \
    file://beam-poweroff.png \
    file://icon-wifi.png \
    file://icon-vpn.png \
    file://windows-vm.desktop \
    file://linux-vm.desktop \
    file://xterm.desktop \
"

DEPENDS += " dconf-native "

FILES_${PN} += "${sysconfdir}/dconf/db/local"
FILES_${PN} += "${datadir}/redfield/beam.png"
FILES_${PN} += "${datadir}/redfield/beam-windows.png"
FILES_${PN} += "${datadir}/redfield/beam-linux.png"
FILES_${PN} += "${datadir}/redfield/beam-firefox.png"
FILES_${PN} += "${datadir}/redfield/beam-poweroff.png"
FILES_${PN} += "${datadir}/redfield/icon-wifi.png"
FILES_${PN} += "${datadir}/redfield/icon-vpn.png"
FILES_${PN} += "${datadir}/applications/windows-vm.desktop"
FILES_${PN} += "${datadir}/applications/linux-vm.desktop"
FILES_${PN} += "${datadir}/applications/xterm.desktop"

do_compile() {
    ${RECIPE_SYSROOT_NATIVE}/${bindir}/dconf compile ${WORKDIR}/local ${WORKDIR}/dconf.d
}

do_install() {
    install -d ${D}/${datadir}/redfield
    install -d ${D}/${datadir}/applications
    install -d ${D}/${sysconfdir}/dconf/db
    install -d ${D}/${sysconfdir}/dconf/profile

    install -m 0644 ${WORKDIR}/local ${D}/${sysconfdir}/dconf/db/local
    install -m 0644 ${WORKDIR}/dconf-profile ${D}/${sysconfdir}/dconf/profile/local

    install -m 0644 ${WORKDIR}/windows-vm.desktop ${D}/${datadir}/applications/windows-vm.desktop
    install -m 0644 ${WORKDIR}/linux-vm.desktop ${D}/${datadir}/applications/linux-vm.desktop
    install -m 0644 ${WORKDIR}/xterm.desktop ${D}/${datadir}/applications/xterm.desktop

    install -m 0644 ${WORKDIR}/beam.png ${D}/${datadir}/redfield/beam.png
    install -m 0644 ${WORKDIR}/beam-windows.png ${D}/${datadir}/redfield/beam-windows.png
    install -m 0644 ${WORKDIR}/beam-linux.png ${D}/${datadir}/redfield/beam-linux.png
    install -m 0644 ${WORKDIR}/beam-firefox.png ${D}/${datadir}/redfield/beam-firefox.png
    install -m 0644 ${WORKDIR}/beam-poweroff.png ${D}/${datadir}/redfield/beam-poweroff.png
    install -m 0644 ${WORKDIR}/icon-wifi.png ${D}/${datadir}/redfield/icon-wifi.png
    install -m 0644 ${WORKDIR}/icon-vpn.png ${D}/${datadir}/redfield/icon-vpn.png
}

