DESCRIPTION = "DOM0 EFI CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://efi.cfg \
"

FILES_${PN} = "/boot/efi.cfg"

do_install() {
    install -D -m 0644 ${WORKDIR}/efi.cfg ${D}/boot/efi.cfg
}

