DESCRIPTION = "PRIME DOM0 IMAGE"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

INITRD_IMAGE = "core-image-minimal"

inherit prime-image

do_bootimg[depends] += "dom0-image-initramfs:do_image_complete"

IMAGE_INSTALL += " \
    coreutils \
    coreutils \
    dash-to-dock \
    dash-to-dock \
    dom0-efi-config \
    dom0-gnome-configs \
    dom0-iptables-configs \
    dom0-modules-configs \
    dom0-networkd-configs \
    dom0-sysctl-configs \
    e2tools \
    flat-remix-gnome \
    gnome-settings-daemon \
    gnome-shell-extensions \
    gnome-tweaks \
    grub-bootconf \
    grub-efi \
    kernel-image-bzimage \
    kernel-module-hid-alps \
    kernel-module-i915 \
    kernel-module-xen-blkback \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
    kernel-module-xen-netback \
    kernel-module-xen-wdt \
    kernel-modules \
    ovmf \
    packagegroup-fonts-truetype-core \
    packagegroup-gnome3-base \
    packagegroup-networkvm \
    packagegroup-storage-driver-modules \
    pciutils \
    plymouth \
    plymouth \
    prime-image-configs \
    qemu \
    seabios \
    shim \
    startx \
    usbutils \
    xen-base \
    xen-efi \
    xen-hypervisor \
    xterm \
    "

image_postprocess_dom0() {
    install -m 0644 ${DEPLOY_DIR_IMAGE}/dom0-image-initramfs-${MACHINE}.cpio.gz ${IMAGE_ROOTFS}/boot/initramfs.gz
    rm ${IMAGE_ROOTFS}/${systemd_unitdir}/system/xendomains.service
    chmod 0755 ${IMAGE_ROOTFS}/${base_libdir}/systemd/systemd-vconsole-setup
    chmod 0755 ${IMAGE_ROOTFS}/${base_libdir}/systemd/systemd-remount-fs
    chmod 0755 ${IMAGE_ROOTFS}/${base_libdir}/systemd/systemd-random-seed
    chmod 0755 ${IMAGE_ROOTFS}/${base_libdir}/systemd/systemd-machined
    chmod 0755 ${IMAGE_ROOTFS}/${base_libdir}/systemd/systemd-timesyncd
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_dom0; \
"
