SUMMARY = "Redfield Toolstack"
HOMEPAGE = "http://www.gitlab.com/redfield/toolstack"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
           gtk+3 \
           grpc \
          "

inherit go pkgconfig

GO_IMPORT = "gitlab.com/redfield/toolstack"
SRC_URI = "git://gitlab.com/redfield/toolstack.git;protocol=ssh;branch=master;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"

SRCREV = "${AUTOREV}"
RDEPENDS_${PN}-dev = "bash make"

GOBUILDMODE = 'exe'

