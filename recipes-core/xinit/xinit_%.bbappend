FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " file://xinitrc.patchx "

do_compile_append() {
	patch -p1 ${B}/xinitrc < ${WORKDIR}/xinitrc.patchx
}
