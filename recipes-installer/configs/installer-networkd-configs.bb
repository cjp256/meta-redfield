DESCRIPTION = "INSTALLER NETWORKD CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://10-wlan0.link \
    file://11-wlan0.network \
    file://11-outside.network \
"

do_install() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-wlan0.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-wlan0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-outside.network ${D}${sysconfdir}/systemd/network
}

RDEPENDS_${PN} = " \
    bash \
"
