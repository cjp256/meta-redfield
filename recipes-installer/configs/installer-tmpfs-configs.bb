DESCRIPTION = "INSTALLER TMPFS CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
    file://etc-lvm.mount \
"

do_install_append() {
    install -d -m 755 ${D}${systemd_system_unitdir}
    install -p -m 644 ${WORKDIR}/etc-lvm.mount ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "etc-lvm.mount"

FILES_${PN} += "${systemd_system_unitdir}"
