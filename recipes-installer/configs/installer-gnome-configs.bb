DESCRIPTION = "Dom0 GNOME Shell Configurations"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://dconf.d/dconf-user.conf \
    file://dconf-profile \
    file://beam.png \
    file://beam-icon.png \
    file://installer.desktop \
"

DEPENDS += " dconf-native "

FILES_${PN} += "${sysconfdir}/dconf/db/local"
FILES_${PN} += "${datadir}/redfield/beam.png"
FILES_${PN} += "${datadir}/redfield/beam-icon.png"
FILES_${PN} += "${datadir}/applications/installer.desktop"

do_compile() {
    ${RECIPE_SYSROOT_NATIVE}/${bindir}/dconf compile ${WORKDIR}/local ${WORKDIR}/dconf.d
}

do_install() {
    install -d ${D}/${datadir}/redfield
    install -d ${D}/${datadir}/applications
    install -d ${D}/${sysconfdir}/dconf/db
    install -d ${D}/${sysconfdir}/dconf/profile

    install -m 0644 ${WORKDIR}/local ${D}/${sysconfdir}/dconf/db/local
    install -m 0644 ${WORKDIR}/dconf-profile ${D}/${sysconfdir}/dconf/profile/local

    install -m 0644 ${WORKDIR}/installer.desktop ${D}/${datadir}/applications/installer.desktop

    install -m 0644 ${WORKDIR}/beam.png ${D}/${datadir}/redfield/beam.png
    install -m 0644 ${WORKDIR}/beam-icon.png ${D}/${datadir}/redfield/beam-icon.png
}

