DESCRIPTION = "A minimal xen image"

INITRD_IMAGE = "dom0-image-initramfs"

inherit prime-image

IMAGE_INSTALL += " \
    coreutils \
    dash-to-dock \
    dosfstools \
    e2fsprogs-mke2fs \
    e2tools \
    flat-remix-gnome \
    gnome-settings-daemon \
    gnome-shell-extensions \
    gnome-tweaks \
    go-toolstack \
    installer-gnome-configs \
    installer-iptables-configs \
    installer-networkd-configs \
    installer-sysctl-configs \
    installer-tmpfs-configs \
    kernel-modules \
    linux-firmware \
    lvm2 \
    packagegroup-fonts-truetype-core \
    packagegroup-gnome3-base \
    packagegroup-network-driver-modules \
    packagegroup-networkvm \
    packagegroup-storage-driver-modules \
    parted \
    pciutils \
    plymouth \
    prime-image-configs \
    startx \
    usbutils \
    wireless-regdb \
    wpa-supplicant \
    xterm \
    "

LICENSE = "MIT"
EFI_PROVIDER = "grub-efi"
SYSTEMD_BOOT_TIMEOUT = "0"

image_postprocess_installer() {
    mkdir -p ${IMAGE_ROOTFS}/images
    mkdir -p ${IMAGE_ROOTFS}/storage

    install -m 0644 ${DEPLOY_DIR_IMAGE}/dom0-image-${MACHINE}.ext4 ${IMAGE_ROOTFS}/images/dom0.ext4
    #install -m 0644 ${DEPLOY_DIR_IMAGE}/ndvm-image-${MACHINE}.ext4 ${IMAGE_ROOTFS}/images/ndvm.ext4
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_installer; \
"
