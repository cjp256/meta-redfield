DESCRIPTION = "Dash to Dock for Gnome Shell"
LICENSE = "CCASA-4.0"

LIC_FILES_CHKSUM = "file://LICENSE;md5=c63c812da95490661796a4bafbf1a658"

SRC_URI = " \
           git://github.com/daniruiz/flat-remix-gnome;protocol=http;branch=master;tag=20181014 \
          "

S = "${WORKDIR}/git"

inherit pkgconfig gettext

FILES_${PN} += "/usr/share/themes"

export DESTDIR="${D}"

do_compile() {
    oe_runmake all
}

do_install() {
    oe_runmake install
}
